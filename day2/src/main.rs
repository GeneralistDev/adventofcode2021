use std::fs;
use std::str;

enum Vector {
    Forward,
    Up,
    Down
}

trait ICommand {
    fn execute(&self, target: &mut dyn IMovable);
}

trait IMovable {
    fn apply(&mut self, direction: &Vector, magnitude: &isize);
}

struct Command {
    direction: Vector,
    magnitude: isize,
}

struct Submarine {
    x: isize, // Horizontal
    y: isize, // Vertical (larger is deeper)
    aim: isize // Aim value
}

impl ICommand for Command {
    fn execute(&self, target: &mut dyn IMovable) {
        target.apply(&self.direction, &self.magnitude);
    }
}

impl IMovable for Submarine {
    fn apply(&mut self, direction: &Vector, magnitude: &isize) {
        match direction {
            Vector::Forward => {
                self.x += magnitude;
                self.y += magnitude * self.aim;
            },
            Vector::Down => {
                self.aim += magnitude;
            },
            Vector::Up => {
                self.aim -= magnitude;
            }
        }
    }
}

fn instructions_to_commands<'a>(lines: str::Lines) -> Vec<Command> {
    let mut commands: Vec<Command> = Vec::new();

    for line in lines {
        let instruction_parts: Vec<&str> = line.split(" ").collect();

        let vector = match instruction_parts[0] {
            "forward" => Vector::Forward,
            "down" => Vector::Down,
            "up" => Vector::Up,
            _ => panic!("Instruction didn't match enum")
        };

        let magnitude = instruction_parts[1].parse::<isize>().unwrap();

        commands.push(Command {
            direction: vector,
            magnitude: magnitude,
        });
    }

    return commands;
}

fn main() {
    let contents = fs::read_to_string("input.txt")
        .expect("Failed to read input file");

    let instructions = contents.lines();

    let mut sub = Submarine {
        x: 0,
        y: 0,
        aim: 0,
    };

    let commands = instructions_to_commands(instructions);

    for command in commands {
        command.execute(&mut sub);
    }

    println!("Result {}", sub.x * sub.y)
}
