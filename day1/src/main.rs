use std::str::Lines;
use std::fs;

fn count_increases(lines: Lines) -> usize {
    let mut prev: Option<isize> = None;

    let mut count_increased: usize = 0;


    let lines: Vec<isize> = lines.map(|x| x.to_string().parse::<isize>().unwrap()).collect();

    // For each sliding window of 3 measurements
    // Current value is the sum of 3 consecutive measurements
    // If we don't have a previous value, we store one
    // If we do have a previous value we compare to see if its an increase
    // If it's an increase we increment the increases by 1
    for i in 0..lines.len() - 2 {
        // Current number is a combination of the current sliding window
        let current = lines[i] + lines[i+1] + lines[i+2];

        match prev {
            None => {
                println!("No previous value");
                prev = Some(current);
            },
            Some(previous) => {
                if current > previous {
                    count_increased += 1;
                }

                prev = Some(current);
            },
        }
    }

    return count_increased;
}

fn main() {
    let contents = fs::read_to_string("input.txt")
        .expect("Something went wrong reading the file");

    // splits the string on new lines (system agnostic)
    let measurements = contents.lines();

    let result = count_increases(measurements);

    println!("Increases: {}", result);
}
