use std::fs;
use std::str;

fn calculate_power_consumption(diag_lines: &Vec<u32>, bits: u32) -> u32 {
    let mut gamma = 0;
    let mut epsilon = 0;

    for shift in 0..bits {
        // Generate a mask with a 1 in the correct position
        let mask = u32::pow(2, shift);

        let mut count_flags = 0;

        for diag_line in diag_lines {
            if diag_line & mask == mask {
                count_flags += 1;
            }
        }

        if count_flags >= (diag_lines.len() / 2) {
            gamma |= mask;
        } else {
            epsilon |= mask;
        }
    }

    println!("Gamma: {}", gamma);
    println!("Epsilon: {}", epsilon);

    return gamma * epsilon;
}

fn filter_rating(oxygen_or_co2_rating: bool, diag_lines: &Vec<u32>, bits: u32) -> u32 {
    let mut filtered_lines = diag_lines.clone();

    // Have to go from left to right
    for shift in (0..bits).rev() {
        let mask = u32::pow(2, shift);

        // Count of rows that have a 1 in this position
        let mut count_flags = 0;

        for filtered_line in &filtered_lines {
            if *filtered_line & mask == mask {
                count_flags += 1;
            }
        }

        match oxygen_or_co2_rating {
            true => {
                // Most common val (1/0)
                if count_flags as f64 >= filtered_lines.len() as f64 / 2.0 {
                    // Filter by `1`
                    filtered_lines = filtered_lines
                        .into_iter()
                        .filter(|item| *item & mask == mask)
                        .collect();
                } else {
                    // Filter by `0`
                    filtered_lines = filtered_lines
                        .into_iter()
                        .filter(|item| *item & mask != mask)
                        .collect();
                }
            },
            false => {
                // Least common val (1/0)
                if (count_flags as f64) < filtered_lines.len() as f64 / 2.0 {
                    // Filter by `1`
                    filtered_lines = filtered_lines
                        .into_iter()
                        .filter(|item| *item & mask == mask)
                        .collect();
                } else {
                    // Filter by `0`
                    filtered_lines = filtered_lines
                        .into_iter()
                        .filter(|item| *item & mask != mask)
                        .collect();
                }
            }
        }

        if filtered_lines.len() == 1 {
            break;
        }
    }

    return filtered_lines[0]
}

fn parse_to_numbers(lines: &mut str::Lines) -> (Vec<u32>, usize) {
    // Clone it so we can use it twice
    let mut cloned_lines = lines.clone();

    let mut diagnostic_lines: Vec<u32> = Vec::new();

    for line in lines {
        diagnostic_lines.push(u32::from_str_radix(line, 2).unwrap())
    }

    let bits = cloned_lines.nth(0).unwrap().len();

    return (diagnostic_lines, bits);
}

fn main() {
    let contents = fs::read_to_string("input.txt")
        .expect("Could not load");

    let mut lines = contents.lines();

    let (diag_lines, bits) = parse_to_numbers(&mut lines);

    let power_consumption = calculate_power_consumption(&diag_lines, bits as u32);

    println!("Power consumption: {}", power_consumption);

    let o_rating = filter_rating(true, &diag_lines, bits as u32);
    let c_rating = filter_rating(false, &diag_lines, bits as u32);

    println!("Oxygen generator rating: {}", o_rating);
    println!("Carbon scrubber rating: {}", c_rating);

    println!("Life support rating: {}", o_rating * c_rating);
}
